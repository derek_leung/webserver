<?php
/**
*   check login status
*/
function checkAuth($cookieName,$cookieValue) {
    $cookie = $_ENV['HTTP_COOKIE'];
    $cookies = array_map('trim', explode('; ', $cookie));
    $authCookie = in_array($cookieName.'='.$cookieValue, $cookies);
    if(!$authCookie) {
        redirectNotAuth();
    } 

}

/**
* clear auth cookie
*/
function logOut($cookiename){
    set_http_status('301 Redirect');
    echo 'set-cookie: '.$cookiename.'=null; max-age=0; path='.'/'.';'."\n";
    set_location('/login.html?logout=true');
}

function redirectNotAuth() {
    set_http_status('301 Not Authorized Redirect');
    set_location('/login.html?unauthorized=true');
}

/**
* get piped post data
*/
function getPostData() {

    while(!feof(STDIN)){
        $postData = fgets(STDIN);
    }

    return $postData;
}

/**
* parse the form data
*/
function parseQueryString($postData) {
    list($username, $password) = explode('&', $postData);
    $username = explode('=', $username)[1];
    $password = explode('=', $password)[1];
    
    return array($username, $password);
}

/** 
* check if the username exist in db
*/
function selectUser($dbh, $username) {
    $query = $dbh->prepare("SELECT rowid FROM users WHERE username=(?)");
    $query->execute(array($username));
    $result = $query->fetchAll();

    return $result;
}

/**
* connect to the database
*/
function sqliteConnect() {
    $dir = 'sqlite:../webapp1/php_db';
    $dbh  = new PDO($dir) or die("cannot open the database");
    
    return $dbh;
}

function set_cookie($cookiename,$cookievalue,$cookietime){
   echo 'set-cookie: '.$cookiename.'='.$cookievalue.'; max-age='.$cookietime.'; path='.'/'.';'."\n";  
}

/**
* redirect
* params:
*   $location (relative url) ex. '/index.html'
*/
function set_location($location) {
    echo 'location: '.$location."\n";
}

/**
* set http status code
*
*params:
* $status (status code) ex. "200 OK"
*/
function set_http_status($status) {
    echo "HTTP/1.1 ".$status."\n";
}

function debug_output() {
    echo json_encode($_ENV);
    $everything = get_defined_vars();
    ksort($everything);
    echo '<pre>';
    print_r($everything);
    echo '</pre>';
}
?>