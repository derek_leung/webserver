#!/usr/bin/env php
<?php
include 'utility.php';

/**
* add a user to the database
*/
function addUser($dbh, $username, $password) {
    $query = $dbh->prepare("INSERT INTO users VALUES (?)");
    $query->execute(array($username));
    
    $userId = selectUser($dbh, $username);
    $query2 = $dbh->prepare("INSERT INTO passwords VALUES (?, ?)");
    $query2->execute(array($password, $userId[0]['rowid']));
}

function main() {
    $postData = getPostData();
    list($username, $password) = parseQueryString($postData);
    $dbh = sqliteConnect();
    addUser($dbh, $username, $password);
    set_http_status('301 redirect');
    set_location('/cgi-scripts/index.php?add="success"');
}

main();
?>
