#!/usr/bin/env php
<?php
include 'utility.php';

/**
* add a user from the database
*/
function deleteUser($dbh, $username, $password) {
    $userId = selectUser($dbh, $username);
    $query2 = $dbh->prepare("DELETE FROM passwords WHERE passworduser=(?)");
    $query2->execute(array($userId[0]['rowid']));
    $query = $dbh->prepare("DELETE FROM users WHERE username=(?)");
    $query->execute(array($username));
}

function main() {
    $postData = getPostData();
    list($username, $password) = parseQueryString($postData);
    $dbh = sqliteConnect();
    deleteUser($dbh, $username, $password);
    set_http_status('200 OK');
}

main();
?>
