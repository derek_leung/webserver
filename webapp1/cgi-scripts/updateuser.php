#!/usr/bin/env php
<?php
include 'utility.php';

/**
* update a user in the database
*/
function updateUser($dbh, $username, $password, $oldusername) {
    $userId = selectUser($dbh, $oldusername);
    $query2 = $dbh->prepare("UPDATE users SET username=(?) WHERE rowid=(?);");
    $query2->execute(array($username, $userId[0]['rowid']));
    $query = $dbh->prepare("UPDATE passwords SET password=(?) WHERE passworduser=(?)");
    $query->execute(array($password, $userId[0]['rowid']));
}

/**
* parse the form data
*/
function parseQueryStringUpdate($postData) {
    list($username, $password, $oldusername) = explode('&', $postData);
    $username = explode('=', $username)[1];
    $password = explode('=', $password)[1];
    $olderusername = explode('=', $oldusername)[1];
    
    return array($username, $password, $olderusername);
}

function main() {
    $postData = getPostData();
    list($username, $password, $oldusername) = parseQueryStringUpdate($postData);
    $dbh = sqliteConnect();
    updateUser($dbh, $username, $password, $oldusername);
    set_http_status('301 redirect');
    set_location('/cgi-scripts/index.php?update="success"');
}

main();
?>