#!/usr/bin/env php
<?php
include 'utility.php';

/**
* get all the rows from the users table in the db
*/
function getUsers($dbh) {
	$query = $dbh->prepare("SELECT username, password FROM users INNER JOIN passwords ON users.rowid=passwords.passworduser");
	$query->execute();
	$result = $query->fetchAll();

	return $result;
}



function main() {
	checkAuth('authenticated', 1);
	$dbh = sqliteConnect();
	$users = getUsers($dbh);
	echo json_encode($users);
}

main();
?>