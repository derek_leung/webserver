#!/usr/bin/env php
<?php
include 'utility.php';

function main() {
  checkAuth('authenticated', 1);
}

main();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- jQuery datatables -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

    <!-- Local CSS -->
      <link type="text/css" rel="stylesheet" href="/assets/stylesheets/application.css" media="all">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Derek Leung's PHP Web App</a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li>
                <div id="logout-form-div">
                    <form id="logout-form" method="post" action="logout.php">
                        <button type="submit" class="btn btn-default">Log Out</button>
                    </form>
                </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div id="message" class="message"></div>
    <div id='content-container' class="container-fluid scrollable">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
              <ul class="nav nav-sidebar">
                <li class="active"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
                <li><a id="add-user" href="#">Add User</a></li>
              </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-8 col-md-offset-0 main">
                <h1>Welcome to the PHP web app. Navigate with the links on the left</h1>
                <h2>Current Users</h2>
                <table id="user-table" class="display" cellspacing="0" width="100%"></table>
            </div>
        </div>
    <div>

    <!-- JQuery -->
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

    <!-- local javascript -->
    <script src="/assets/javascript/application.js"></script>
  </body>
</html>


