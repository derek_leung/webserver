#!/usr/bin/env php
<?php
include 'utility.php';
/** 
* check if the username and password combo exist in db
*/
function selectPassword($dbh, $userId) {
    $query = $dbh->prepare("SELECT rowid, password FROM passwords WHERE passworduser=(?)");
    $query->execute(array($userId));
    $result = $query->fetchAll();

    return $result;
}

/**
* verify password is correct
*/
function verifyPassword($password, $actual_password) {
    if ($password == $actual_password) {
        return true;
    }
    
    return false;
}

function main() {
    global $postData;
    global $username;
    global $password;
    global $userResult;
    global $passwordResult;
    global $message; 
    global $userId;
    global $password;
    global $isValid;
    global $cookieName;
    global $cookieValue;
    $message = 'Username or password incorrect.';
    $postData = getPostData();
    list($username, $password) = parseQueryString($postData);
    $dbh = sqliteConnect();
    $userResult = selectUser($dbh, $username);

    if (count($userResult) > 0) {
        //username exists
        $userId = $userResult[0]['rowid'];
        $passwordResult = selectPassword($dbh, $userId);
        if (count($passwordResult) > 0) {
            $actual_password = $passwordResult[0]['password'];
            $isValid = verifyPassword($password, $actual_password);
            if ($isValid) {
                $cookieName = "authenticated";
                $cookieValue = 1;
                set_http_status('301 redirect');
                set_cookie($cookieName, $cookieValue, 86400);
                set_location('index.php');
                exit();
            }
        }   
    }
    set_http_status('301 redirect');
    set_location('/login.html?invalid=true');
}

main();
debug_output();
?>

<div>
incorrect username and password
</div>
