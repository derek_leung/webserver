var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function getData(){
    $.ajaxSetup({ cache: false });
    $.ajax({
        type: "get",
        url: "/cgi-scripts/getdata.php",
        dataType: 'json',
        async: false,
        success: function(dataSet) {
            buildTable(dataSet);
            console.log(data);
            addUserCallback(dataSet);
            updateUserCallback(dataSet);
        }
    });
}

function buildTable(dataSet) {
    table = $('#user-table').DataTable({
        data: dataSet,
        columns: [
            { title: "Username", data: "username" },
            { title: "Password", data: "password" },
            { title: "Actions", data: null, defaultContent: '<button class="update btn btn-info">Update</button><button class="delete btn btn-danger">Delete</button>'},
        ]

    });
}

function addUserCallback(dataSet) {
    $(document).off("click", "#add-submit").on("click", "#add-submit", function(e){
        var username = $('#username').val();
        var password = $('#password').val();
        var duplicate = false;
        console.log(dataSet);
        $.each(dataSet, function(index) {
            if (dataSet[index]['username'] == username) {
                duplicate = true;
            }
        });
        if (!duplicate) {
            $.ajax({
                type: "post",
                url: "/cgi-scripts/adduser.php",
                async: false,
                data: {
                    "username":username,
                    "password":password,
                },
                success: function() {
                    location.reload(); 
                }
            });
        } else {
            $('#message').html('<h3>' + 'username already in use.' + '</h3>');
        }
    });
}

function updateUserCallback(dataSet) {
    $(document).off("click", "#update-submit").on("click", "#update-submit", function(e){
        var oldusername = $('#old').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var duplicate = false;

        $.each(dataSet, function(index) {
            if (dataSet[index]['username'] == username) {
                duplicate = true;
            }
        });
        if (!duplicate) {
            $.ajax({
                type: "post",
                url: "/cgi-scripts/updateuser.php",
                async: false,
                data: {
                    "username":username,
                    "password":password,
                    "oldusername": oldusername,
                },
                success: function() {
                    location.reload(); 
                }
            });
        } else {
            $('#message').html('<h3>' + 'username already in use.' + '</h3>');
        }
    });
}

function deleteUser(el) {
    console.log($(el).children().eq(0).text());
    console.log($(el).children().eq(1).text());
    username = $(el).children().eq(0).text();
    password = $(el).children().eq(1).text();
    $.ajax({
        type: "post",
        url: "/cgi-scripts/deleteuser.php",
        data: {
            "username":username,
            "password":password,
        },
        success: function(response) {
            console.log('deleted')
            table.destroy();
            getData();
        },
    });
}

$(document).ready( function() {
    var table;
    window.data = '';
    getData();

    var invalid = getUrlParameter('invalid');
    var logout = getUrlParameter('logout');
    var add = getUrlParameter('add');
    var unauthorized = getUrlParameter('unauthorized');
    console.log(add);

    var message = '';

    if (invalid) {
        message = "Invalid username and/or password.";
    } else if (logout) {
        message = "Successfully logged out.";
    } else if (unauthorized) {
        message = "Unauthorized. Please log in.";
    }
    $('#message').html('<h3>' + message + '</h3>');

    $('#add-user').click(function() {
       $('.main').html(
           '<div id="add-form-panel" class="panel panel-primary user-form-panel">' +
            '<div class="panel-heading">' +
            '<h3 class="panel-title">This will add a user to the database</h3>' +
            '</div>' +
           '<div class="panel-body">' +
            '<div id="add-form-div" class="user-form-div">' +
               '<div id="add-user-form" method="post">' +
                    '<div class="form-group">' +
                        '<div class="input-group">' +
                            '<input id="username" name="username" type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">' +
                        '</div>' +
                        '<div class="input-group">' +
                            '<input id="password" name="password" type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">' +
                        '</div>' +
                        '<div id="add-submit" class="btn btn-primary user-form-submit">Add</div>' +
                    '</div>' +
               '</div>' +
               '</div>' +
           '</div>' +
           '</div>'
        );
    });



    $(document).on("click", ".update", function(event) {
        $('.main').html(
           '<div id="update-form-panel" class="panel panel-primary user-form-panel">' +
            '<div class="panel-heading">' +
            '<h3 class="panel-title">This will update this user in the database</h3>' +
            '</div>' +
           '<div class="panel-body">' +
            '<div id="update-form-div" class="user-form-div">' +
               '<div id="update-user-form" method="post">' +
                    '<div class="form-group">' +
                        '<div class="input-group">' +
                            '<input type="hidden" id="old" name="old">' +
                            '<input id="username" name="username" type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">' +
                        '</div>' +
                        '<div class="input-group">' +
                            '<input id="password" name="password" type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">' +
                        '</div>' +
                        '<button id="update-submit" class="btn btn-primary user-form-submit">Update</button>' +
                    '</div>' +
               '</div>' +
               '</div>' +
           '</div>' +
           '</div>'
        );
        var el = $(event.target).parent().parent();
        $('#old').val($(el).children().eq(0).text());
        $('#username').val($(el).children().eq(0).text());
        $('#password').val($(el).children().eq(1).text());
    });


    $(document).on("click", ".delete", function(event) {
        console.log($(event.target).parent().parent());
        deleteUser($(event.target).parent().parent());
    });



});
