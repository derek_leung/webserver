import sys
import socket
import re
import json
import datetime
import os.path
import threading
import subprocess


class WebServer():
    def __init__(self, ipaddresslisten, portlisten, allowedmethods, approotfolder, requestlog, badrequestlog, script_dir):
        self.script_dir = os.path.abspath(script_dir)
        self.host = ipaddresslisten
        self.port = int(portlisten)
        self.methods = allowedmethods
        self.methods = [str(item).upper() for item in self.methods]
        self.app_root_folder = approotfolder
        self.request_log = requestlog
        self.bad_request_log = badrequestlog
        self.listen_socket = ''

    def run(self):
        #AF_INET = IPv4, SOCK_STREAM = TCP
        self.listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.listen_socket.bind((self.host, self.port))
        self.listen_socket.listen(0)  # # of simultaneous connections permitted by server

        self.init_output()
        num=0

        while True:
            client_connection, client_address = self.listen_socket.accept()
            num += 1
            handler_thread = threading.Thread(target=self.conn_handler, args=(client_connection, num))
            handler_thread.start()

    def conn_handler(self, client_connection, num):
            try:
                request = client_connection.recv(1024)  #blocking
                print request
                if request:
                    http_response = self.respond(request)
                    client_connection.sendall(http_response)
                    client_connection.close()
                    print 'connection closed'
                else:
                    raise Exception('Client disconnected')
            except:
                client_connection.close()
                return False

    def respond(self, request):
        #match = re.match('GET /get HTTP/1', request)
        match = re.match('(GET|POST|PUT|DELETE|CONNECT)\s+([^?\s]+).*(HTTP/.*)', request)
        #can just split using python by line then by space to get the entire request
        lines = request.split('\n')
        http_response = 'HTTP/1.1 404 Not Found\n\n404 Page Not Found\n'
        if match and any(line.lower().startswith('host: ') for line in lines):
            method = match.group(0).split()[0].upper()
            if method in self.methods:
                if method == 'GET':
                    http_response = self.get(request)
                if method == 'POST':
                    http_response = self.post(request)
                if method == 'PUT':
                    http_response = self.put(request)
                if method == 'DELETE':
                    http_response = self.delete(request)
                if method == 'CONNECT':
                    http_response = self.connect(request)

            self.log_request(request)
        else:
            #log the bad request
            self.log_bad_request(request)
            http_response = 'HTTP/1.1 400 Bad Request\n\n400Bad Request\n'

        return http_response

    """
        HTTP GET method. Return requested content from specified source if it exists.

        params:
            request (string)
    """
    def get(self, request):
        request_lines = request.split('\n')
        requested_content = request_lines[0].split()[1][1:].split('?')[0]
        if len(request_lines[0].split()[1][1:].split('?')) == 2:
            query_string = request_lines[0].split()[1][1:].split('?')[1]
        http_response = 'HTTP/1.1 404 Not Found\n\n404 Page Not Found\n'
        requested_content_path = os.path.join(self.app_root_folder, requested_content)
        if os.path.isfile(requested_content_path):
            dir_path = os.path.dirname(os.path.realpath(requested_content_path))
            print dir_path
            if dir_path == self.script_dir: #cgi script
                if os.access(requested_content_path, os.X_OK):
                    http_response = self.cgi(request)

                else:
                    http_response = 'HTTP/1.1 403 Forbidden\n\n403 Forbidden\n'
            else:  # not a script
                if os.access(requested_content_path, os.R_OK):
                    with open(requested_content_path, 'r') as f:
                        content = f.read()

                    content_type = self.get_content_type(requested_content_path)
                    http_response = 'HTTP/1.1 200 OK\nContent-Type: ' + content_type + '\n\n' + content
                else:
                    http_response = 'HTTP/1.1 403 Forbidden\n\n403 Forbidden\n'

        return http_response

    def post(self, request):
        request_lines = request.split('\n')
        requested_content = request_lines[0].split()[1][1:].split('?')[0]
        if len(request_lines[0].split()[1][1:].split('?')) == 2:
            query_string = request_lines[0].split()[1][1:].split('?')[1]
        http_response = 'HTTP/1.1 404 Not Found\n\n404 Page Not Found\n'
        requested_content_path = os.path.join(self.app_root_folder, requested_content)
        if os.path.isfile(requested_content_path):
            dir_path = os.path.dirname(os.path.realpath(requested_content_path))
            if dir_path == self.script_dir: #cgi script
                if os.access(requested_content_path, os.X_OK):
                    length = ''
                    for i in range(0, len(request_lines)):
                        if request_lines[i].lower().startswith('content-length: '):
                            length = request_lines[i].split(': ')[1]

                    if length != '':
                        http_response = self.cgi(request)
                    else:
                        http_response = 'HTTP/1.1 411 Length Required\n\n411 Length Required\n'
                else:
                    http_response = 'HTTP/1.1 403 Forbidden\n\n403 Forbidden\n'

            else: #not a script
                if os.access(requested_content_path, os.R_OK):
                    with open(requested_content_path, 'r') as f:
                        content = f.read()

                    content_type = self.get_content_type(requested_content_path)
                    http_response = 'HTTP/1.1 200 OK\nContent-Type: ' + content_type + '\n\n' + content
                else:
                    http_response = 'HTTP/1.1 403 Forbidden\n\n403 Forbidden\n'

        return http_response

    def put(self, request):
        request_lines = request.split('\n')
        body = re.split('\n\s*\n', request)[1]
        requested_content = request_lines[0].split()[1][1:].split('?')[0]
        if len(request_lines[0].split()[1][1:].split('?')) == 2:
            query_string = request_lines[0].split()[1][1:].split('?')[1]
        http_response = 'HTTP/1.1 404 Not Found\n\n404 Page Not Found\n'
        requested_content_path = os.path.join(self.app_root_folder, requested_content)
        if os.path.isfile(requested_content_path):
            if os.access(requested_content_path, os.W_OK):
                with open(requested_content_path, 'a') as f:
                    f.write(body + '\n')
                http_response = 'HTTP/1.1 200 OK\n\nPUT method successful\n'
            else:
                http_response = 'HTTP/1.1 403 Forbidden\n\n403 Forbidden\n'
        return http_response

    """
        Delete the specified resource

        params:
            request (string)
    """
    def delete(self, request):
        request_lines = request.split('\n')
        requested_content = request_lines[0].split()[1][1:].split('?')[0]
        if len(request_lines[0].split()[1][1:].split('?')) == 2:
            query_string = request_lines[0].split()[1][1:].split('?')[1]
        http_response = 'HTTP/1.1 404 Not Found\n\n404 Page Not Found\n'
        requested_content_path = os.path.join(self.app_root_folder, requested_content)
        if os.path.isfile(requested_content_path):
            if os.access(requested_content_path, os.W_OK):
                os.remove(requested_content_path)
                http_response = 'HTTP/1.1 200 OK\n\nDELETE method successful\n'
            else:
                http_response = 'HTTP/1.1 403 Forbidden\n\n403 Forbidden\n'

        return http_response

    """
    connect to given proxy
    """
    def connect(self, request):
        request_lines = request.split('\n')
        requested_content = request_lines[0].split()[1][1:].split('?')[0]
        query_string = ''
        if len(request_lines[0].split()[1][1:].split('?')) == 2:
            query_string = request_lines[0].split()[1][1:].split('?')[1]
        connect_address = request_lines[0].split()[1][1:]
        (host, port) = connect_address.split(':')
        print 'Handling connect request to %s' % connect_address

        try:
            soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            soc.connect((host, int(port)))
            print 'Connection opened at %s' % connect_address
            http_response = 'HTTP/1.1 200 Connection established\n\n'
        except Exception as e:
            print e
            http_response = 'HTTP/1.1 500 Internal Server Error\n\n'

        return http_response

    def cgi(self, request):
        post_data = ''
        body = re.split('\n\s*\n', request)
        if len(body) == 2:
            post_data = body[1]
        http_response = 'HTTP/1.1 500 Internal Server Error'
        request_lines = request.split('\n')
        request_method = request_lines[0].split()[0]
        requested_content = request_lines[0].split()[1][1:].split('?')[0]
        requested_content_path = os.path.join(self.app_root_folder, requested_content)
        query_string = ''
        if len(request_lines[0].split()[1][1:].split('?')) == 2:
            query_string = request_lines[0].split()[1][1:].split('?')[1]
        host = ''
        accept = []
        co = []
        content_type = ''
        length = ''
        referer = ''
        ua = ''
        for i in range(0, len(request_lines)):
            if request_lines[i].lower().startswith('host: '):
                host = request_lines[i].split(': ')[1]
            if request_lines[i].lower().startswith('content-type: '):
                content_type = request_lines[i].split(': ')[1]
            if request_lines[i].lower().startswith('content-length: '):
                length = request_lines[i].split(': ')[1]
            if request_lines[i].lower().startswith('referer: '):
                referer = request_lines[i].split(': ')[1]
            if request_lines[i].lower().startswith('accept: '):
                accept.append(request_lines[i].split(': ')[1])
            if request_lines[i].lower().startswith('user-agent: '):
                ua = request_lines[i].split(': ')[1]
            if request_lines[i].lower().startswith('cookie: '):
                co.append(request_lines[i].split(': ')[1])

        decoded_query_string = query_string.replace('+', ' ')

        env = {}
        env['REDIRECT_STATUS'] = '200'
        env['SERVER_SOFTWARE'] = '1.0'
        env['SERVER_NAME'] = 'DerekServer'
        env['GATEWAY_INTERFACE'] = 'CGI/1.1'
        env['SERVER_PROTOCOL'] = '1.1'
        env['SERVER_PORT'] = str(self.port)
        env['REQUEST_METHOD'] = request_method
        env['PATH_INFO'] = requested_content_path
        env['SCRIPT_NAME'] = os.path.basename(requested_content_path)
        if query_string:
            env['QUERY_STRING'] = query_string
        env['REMOTE_HOST'] = self.host
        env['REMOTE_ADDR'] = self.host

        if content_type:
            env['CONTENT_TYPE'] = content_type
        else:
            env['CONTENT_TYPE'] = 'text/html'
        if length:
            env['CONTENT_LENGTH'] = length
        if referer:
            env['HTTP_REFERER'] = referer
        env['HTTP_ACCEPT'] = ','.join(accept)
        if ua:
            env['HTTP_USER_AGENT'] = ua
        if len(co) > 0:
            env['HTTP_COOKIE'] = ', '.join(co)
        for k in ('QUERY_STRING', 'REMOTE_HOST', 'CONTENT_LENGTH',
                  'HTTP_USER_AGENT', 'HTTP_COOKIE', 'HTTP_REFERER'):
            env.setdefault(k, "")
        os.environ.update(env)

        args = [str(requested_content_path)]
        if '=' not in decoded_query_string and len(decoded_query_string) > 0:
            args.append(decoded_query_string)

        try:

            p = subprocess.Popen(args, env=os.environ, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            if request_lines[0].split()[0].lower() == 'post':
                output = p.communicate(post_data)[0]
            else:
                output = p.communicate()[0]

            http_response = output

        except Exception as e:
            print e.message
            http_response = 'HTTP/1.1 500 Internal Server Error\n\n500 Internal Server Error\n'

        return http_response

    """
        get the content type for the response
    """
    def get_content_type(self, requested_content_path):
        content_type = 'text/html'
        """
        mime = magic.Magic(mime=True)
        if mime.from_file(requested_content_path):
            content_type = mime.from_file(requested_content_path)
        """

        if requested_content_path.endswith('.css'):
            content_type = 'text/css'
        if requested_content_path.endswith('.html'):
            content_type = 'text/html'
        if requested_content_path.endswith('.js'):
            content_type = 'application/javascript'

        return content_type

    """
        log a request to the request log
    """
    def log_request(self, request):
        try:
            with open(self.request_log, "a") as f:
                timestamp = datetime.datetime.utcnow()
                f.write('[' + str(timestamp) + '] ' + str(request) + '\n\n')

        except Exception as e:
            print e


    """
        log a bad request to the bad request log
    """
    def log_bad_request(self, request):
        try:
            with open(self.bad_request_log, "a") as f:
                timestamp = datetime.datetime.utcnow()
                f.write('[' + str(timestamp) + '] ' + str(request) + '\n\n')

        except Exception as e:
            print e


    """
        print server configuration to console
    """
    def init_output(self):
        print '================================================================='
        print 'Server initialized with the following configuration...'
        print 'Host: ' + self.host
        print 'Port: ' + str(self.port)
        print 'Allowed Methods: ' + ' '.join([str(item) for item in self.methods])
        print 'App Root Folder: ' + self.app_root_folder
        print 'Request Log: ' + self.request_log
        print 'Bad Request Log: ' + self.bad_request_log
        print '=================================================================\r\n'
        print 'Serving HTTP on port %s ...' % self.port

def config(config_file):
    try:
        with open(config_file) as data_file:
            data = json.load(data_file)['settings'][0]
            ws = WebServer(
                data['ip-address-listen'],
                data['port-listen'],
                data['allowed-methods'],
                data['app-root-folder'],
                data['request-log'],
                data['bad-request-log'],
                data['cgi-directory']
            )

        return ws

    except Exception as e:
        print e


def usage():
    print "Usage: python web_server.py [config.json file]"


def main():
    if len(sys.argv) == 2:
        ws = config(sys.argv[1])
        ws.run()

    else:
        usage()

if __name__ == '__main__':
    main()